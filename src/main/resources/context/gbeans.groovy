package edu.spring.talk.gbeans
import edu.spring.talk.gbeans.inner.IGADrivenBean
import org.springframework.core.io.ClassPathResource

def defaultScope = 'prototype'
def properties = new Properties()
properties.load(new ClassPathResource('application.properties').inputStream);

beans {
    xmlns([ctx:'http://www.springframework.org/schema/context'])
    ctx.'component-scan'('base-package':properties.basePackage)

    //gdbean GDrivenBean, name:'testname', desc:'testdesc'
    if (environment.activeProfiles.contains("dev")) {
        gdbean GDrivenBean, name:'testname', desc:'testdesc'
    } else {
        gdbean GDrivenBean, name:'prodname', desc:'proddesc'
    }
    gdbeanprops(GDrivenBean){
        name = 'propname'
        desc = '${test.desc}'
    }
    igadbean(IGADrivenBean) {bean ->
        bean.scope = defaultScope

        reference = ref 'gdbeanprops'
        s_reference = gdbean
    }
}
