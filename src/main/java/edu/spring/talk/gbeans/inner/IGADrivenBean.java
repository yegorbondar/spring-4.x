package edu.spring.talk.gbeans.inner;

import edu.spring.talk.gbeans.GDrivenBean;
import org.springframework.stereotype.Component;

public class IGADrivenBean {

    GDrivenBean reference;
    GDrivenBean s_reference;

    public GDrivenBean getReference() {
        return reference;
    }

    public void setReference(GDrivenBean reference) {
        this.reference = reference;
    }

    public GDrivenBean getS_reference() {
        return s_reference;
    }

    public void setS_reference(GDrivenBean s_reference) {
        this.s_reference = s_reference;
    }
}
