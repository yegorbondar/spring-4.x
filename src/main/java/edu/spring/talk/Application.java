package edu.spring.talk;

import edu.spring.talk.gbeans.GDrivenBean;
import edu.spring.talk.gbeans.inner.IGADrivenBean;
import edu.spring.talk.jbeans.JDrivenBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.*;

@Configuration
//@ComponentScan
@EnableAutoConfiguration
@PropertySources({
    @PropertySource(value = "config.properties"),
    @PropertySource(value = "config_repeat.properties", ignoreResourceNotFound = true)
})
@ImportResource(value = "classpath:/context/gbeans.groovy")
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    @Bean
    JDrivenBean jDrivenBean(@Qualifier("gdbean")GDrivenBean gdbean){
        return new JDrivenBean("t");
    }

    @Bean
    JDrivenBean jDrivenBeanProp(@Qualifier("gdbeanprops")GDrivenBean gdbean){
        return new JDrivenBean("");
    }

    @Bean
    JDrivenBean jDrivenBeanInner(IGADrivenBean bean){
        return new JDrivenBean("");
    }
}
