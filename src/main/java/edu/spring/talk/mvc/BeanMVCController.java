package edu.spring.talk.mvc;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Scope("request")
@RequestMapping("/mvc")
public class BeanMVCController {

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("message", "test");
        return "test";
    }

    @RequestMapping(value = "/url/{path}", method = RequestMethod.GET)
    public String path(@PathVariable String path, @RequestParam("param") String param){
        return "test";
    }

}
