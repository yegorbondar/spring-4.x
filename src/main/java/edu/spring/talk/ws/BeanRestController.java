package edu.spring.talk.ws;

import edu.spring.talk.annotation.WebServiceController;
import edu.spring.talk.gbeans.GDrivenBean;
import edu.spring.talk.jbeans.JDrivenBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@WebServiceController
public class BeanRestController {

    @Autowired
    @Qualifier("gdbean")
    private GDrivenBean bean;

    @Autowired
    private JDrivenBean<String> genericBean;

    @RequestMapping(value = "/bean/{test}/{name}", method = RequestMethod.GET)
    public GDrivenBean getBean(@PathVariable String name, @PathVariable String test,
                               @RequestParam Optional<String> desc){
        bean.setName(name);
        bean.setDesc(desc.orElse("default"));
        AsyncRestTemplate a = new AsyncRestTemplate();
        return bean;
    }

    @RequestMapping(value = "/bean/generic", method = RequestMethod.GET)
    public String genericType(){
        return genericBean.getClass().getName();
    }
}
