package edu.spring.talk.annotation;

import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping("/webservice")
@RestController
@Scope("request")
@Transactional(isolation = Isolation.DEFAULT)
public @interface WebServiceController {
    Propagation propagation() default Propagation.REQUIRES_NEW;
    String [] headers() default {};
}
