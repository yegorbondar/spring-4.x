package edu.spring.talk.jbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component("jstringbean")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JADrivenBean extends JDrivenBean<String> {
    public JADrivenBean(String t) {
        super(t);
    }
}
