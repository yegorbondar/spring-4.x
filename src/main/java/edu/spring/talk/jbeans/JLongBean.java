package edu.spring.talk.jbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;


@Component(value = "jlongbean")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JLongBean extends JDrivenBean<Long>{
    public JLongBean(String t) {
        super(t);
    }
}
