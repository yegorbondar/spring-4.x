package edu.spring.talk.jbeans;

import edu.spring.talk.gbeans.GDrivenBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class JDrivenBean<T> {

    T value;

    @Autowired
    List<GDrivenBean> gDrivenBean;

    public JDrivenBean(String t){
        System.out.println("JDrivenBean<T> ctor");
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public List<GDrivenBean> getgDrivenBean() {
        return gDrivenBean;
    }

    public void setgDrivenBean(List<GDrivenBean> gDrivenBean) {
        this.gDrivenBean = gDrivenBean;
    }
}
